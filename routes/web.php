<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::middleware(['auth'])->prefix('user')->group(function () {

    Route::get('auth/details','UserController@authDetails')->name('user.auth.details');
    Route::get('{userId]/details','UserController@getUserDetails')->name('user.details');
    Route::get('list','UserController@listAllUsers')->name('user.list');
    Route::get('add','UserController@addUser')->name('user.add');
    Route::post('add','UserController@storeAddedUser')->name('user.add.post');
});
