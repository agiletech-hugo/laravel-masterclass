<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'Masterclass',
            'email' => 'Masterclass@gmail.com',
            'password' => bcrypt('masterclass'),
        ]);

        factory(App\User::class, 50)->create();

    }
}
