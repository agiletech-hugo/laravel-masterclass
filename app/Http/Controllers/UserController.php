<?php

namespace App\Http\Controllers;

use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function listAllUsers() {
        $data = array(
            "users" => User::all()
        );

        return view('user.list')->with(compact('data'));
    }

    public function authDetails() {
        return view('user.auth.details');
    }

    public function getUserDetails(Request $request, $userId) {
        $user = User::findOrFail($userId);

        $data = array(
            "user" => $user
        );

        return view('user.details')->with(compact('data'));

    }

    public function addUser() {
        return view('user.add');
    }

    public function storeAddedUser(Request $request) {
        $request->validate([
            'name' => 'required|string',
            'email' => 'required|email|unique',
            'password' => 'required',
        ]);

        $user = new User();
        $user->name = $request->get('name');
        $user->email = $request->get('email');
        $user->password = bcrypt($request->get('password'));
        $user->email_verified_at = Carbon::now();
        $user->created_at = Carbon::now();
        $user->updated_at = Carbon::now();
        $user->save();


        return redirect(route('user.details',['userId' =>$user->id ]));

    }

}
